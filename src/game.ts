import "core-js/stable"
import "regenerator-runtime/runtime"
import "reflect-metadata"

import { getContainer } from "./inversify.config"
import { MainController } from "./main-controller"


async function main() {

    let container = getContainer()

    let controller = container.get(MainController)

    await controller.run()

}

main()

