interface StrikeZone {

    xMin:number 
    xMax:number

    yMin:number
    yMax:number

    xStrikeMin:number
    xStrikeMax:number 

    yStrikeMin:number
    yStrikeMax:number 

}

export {
    StrikeZone
}