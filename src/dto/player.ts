import { Handedness } from "./handedness";

interface Player {

    personId?:string

    //Offense
    hitting?: {

        contact:number
        power:number
        gapPower:number
        eye:number
        
        speed:number 
        baserunning:number

        flyball:number //higher is more flyballs
        pullHitter:number //higher is more to same field

    }

    //Defense
    defense?: {
        arm:number
        range:number

        position: {
            catcher:number 
            first:number
            second:number
            third:number
            shortstop:number
            left:number
            center:number
            right:number
        }

    }

    //Person
    player?: {
        health:number
    }
    
    //Pitching
    pitching?: {

        stamina:number
        control:number
        power:number
        movement:number

        flyball:number //higher is more flyballs


        pitches?: {
            change:number
            sinker:number
            curve:number
            circleChange:number
            cutter:number
            forkBall:number
            fourSeamFastball:number
            knuckleball:number
            knucklecurve:number
            screwball:number
            slider:number
            splitter:number
            twoSeamFastball:number
        }
    }

    batSide?: Handedness
    throwSide?:Handedness
    
}




export { Player }







//All
    // hit?:number
    // k?:number
    // out?:number
    // sf?:number

    // bb?:number
    // single?:number
    // double?:number
    // triple?:number
    // hr?:number


    // //Left 
    // leftHit?:number
    // leftK?:number
    // leftOut?:number
    // leftSf?:number

    // leftBb?:number
    // leftSingle?:number
    // leftDouble?:number
    // leftTriple?:number
    // leftHr?:number

    // //Right
    // rightHit?:number
    // rightK?:number
    // rightOut?:number
    // rightSf?:number

    // rightBb?:number
    // rightSingle?:number
    // rightDouble?:number
    // rightTriple?:number
    // rightHr?:number
// 