enum Handedness {
    LEFT = "left",
    RIGHT = "right",
    SWITCH = "switch"
}

export {
    Handedness
}