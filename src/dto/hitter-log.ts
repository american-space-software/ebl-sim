interface HitterLog {

    hitterId:number

    games:number
    pa:number

    atBats:number
    hits:number

    singles:number
    doubles:number
    triples:number
    homeRuns:number
    runs:number
    rbi:number
    bb:number
    sb:number
    cs:number
    hbp:number
    so:number
    lob:number
    sacBunts:number
    sacFlys:number

    groundOuts:number
    flyOuts:number
    lineOuts:number
    
    groundBalls:number
    lineDrives:number
    flyBalls:number


    gidp:number
    po:number
    assists:number
    e:number


    //Calculated stats
    avg:number
    ibb:number
    obp:number
    slg:number
    ops:number

    tb:number


    wOba:number
    wFda:number

    soPercent:number
    bbPercent:number

    iso:number

}

export {
    HitterLog
}