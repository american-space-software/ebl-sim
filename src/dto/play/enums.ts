
enum Hardness {
    SOFT = "soft",
    HARD = "hard",
    MEDIUM = "medium"
}

enum Trajectory {
    POPUP = "popup",
    LINE_DRIVE = "line_drive",
    GROUND_BALL = "ground_ball",
    FLY_BALL = "fly_ball",

    BUNT_POPUP = "bunt_popup",
    BUNT_LINE_DRIVE = "bunt_line_drive",
    BUNT_GROUNDER = "bunt_grounder"
}

enum PitchType {
    CHANGEUP = "changeup",
    CURVEBALL = "curveball",
    CUTTER = "cutter",
    FORKBALL = "forkball",
    FOUR_SEAM_FASTBALL = "four-seam-fastball",
    SINKER = "sinker",
    SLIDER = "slider",
    SPLITTER = "splitter",
    TWO_SEAM_FASTBALL = "two-seam-fastball"
}


enum PlayEventType {
    PICKOFF = "pickoff",
    PITCH = "pitch",
    // NO_PITCH = "no_pitch",
    ACTION = "action"
}

export {
    Hardness, PitchType, PlayEventType, Trajectory
}