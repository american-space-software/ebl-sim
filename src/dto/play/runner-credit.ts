
interface RunnerCredit {

    //relationships
    gameId:number 
    runnerId:number

    //movement
    fielderId:number

    positionCode:string 
    positionName:PositionName 

    credit: Credit
    
}

enum PositionName {
    CATCHER = "Catcher",
    FIRST_BASE = "First Base",
    SECOND_BASE = "Second Base",
    SHORTSTOP = "Shortstop",
    OUTFIELDER = "Outfielder",
    THIRD_BASE = "Third Base",
    PITCHER = "Pitcher"
}

enum Credit {
    C_CATCHER_INTERFERENCE = "c_catcher_interf",
    F_ASSIST = "f_assist",
    F_ASSIST_OF = "f_assist_of",
    F_DEFLECTION = "f_deflection",
    F_ERROR_DROPPED_BALL = "f_error_dropped_ball",
    F_FIELDED_BALL = "f_fielded_ball",
    F_FIELDING_ERROR = "f_fielding_error",
    F_INTERFERENCE = "f_interference",
    F_PUTOUT = "f_putout",
    F_THROWING_ERROR = "f_throwing_error",
    F_TOUCH = "f_touch"
}

export {
    RunnerCredit
}