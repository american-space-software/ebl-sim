
interface Runner {

    //relationships
    gameId:number 

    playId:number
    playEventId: number 

    runnerId:number
    responsiblePitcherId:number

    //movement
    originBase: Base

    start: Base
    end: Base
    
    outBase: Base
    
    isOut: boolean 
    outNumber: string

    //Details
    event: RunnerEvent

    movementReason: MovementReason

    isScoringEvent: boolean,

    rbi: boolean,

    earned: boolean,
    teamUnearned: boolean,

    lastUpdated?:Date
    dateCreated?:Date

}


enum RunnerEvent {
    BALK = "Balk",
    BATTER_OUT = "Batter Out",
    BUNT_GROUNDOUT = "Bunt Groundout",
    BUNT_LINEOUT = "Bunt Lineout",
    BUNT_POP_OUT = "Bunt Pop Out",
    CATCHER_INTERFERENCE = "Catcher Interference",
    CAUGHT_STEALING_2B = "Caught Stealing 2B",
    CAUGHT_STEALING_3B = "Caught Stealing 3B",
    CAUGHT_STEALING_HOME = "Caught Stealing Home",
    DEFENSIVE_INDIFF = "Defensive Indiff",
    DOUBLE = "Double",
    DOUBLE_PLAY = "Double Play",
    ERROR = "Error",
    FIELD_ERROR = "Field Error",
    FIELD_OUT = "Field Out",
    FIELDERS_CHOICE = "Fielders Choice",
    FIELDERS_CHOICE_OUT = "Fielders Choice Out",
    FLYOUT = "Flyout",
    FORCEOUT = "Forceout",
    GROUNDED_INTO_DP = "Grounded Into DP",
    GROUNDOUT = "Groundout",
    HIT_BY_PITCH = "Hit By Pitch",
    HOME_RUN = "Home Run",
    INTENT_WALK = "Intent Walk",
    LINEOUT = "Lineout",
    OTHER_ADVANCE = "Other Advance",
    PASSED_BALL = "Passed Ball",
    PICKOFF_1B = "Pickoff 1B",
    PICOFF_2B = "Pickoff 2B",
    PICKOFF_3B = "Pickoff 3B",
    PICKOFF_CAUGHT_STEALING_2B = "Pickoff Caught Stealing 2B",
    PICKOFF_CAUGHT_STEALING_3B = "Pickoff Caught Stealing 3B",
    PICKOFF_CAUGHT_STEALING_HOME = "Pickoff Caught Stealing Home",
    PICKOFF_ERROR_1B = "Pickoff Error 1B",
    PICKOFF_ERROR_2B = "Pickoff Error 2B",
    PICKOFF_ERROR_3B = "Pickoff Error 3B",
    POP_OUT = "Pop Out",
    RUNNER_OUT = "Runner Out",
    SAC_BUNT = "Sac Bunt",
    SAC_BUNT_DOUBLE_PLAY = "Sac Bunt Double Play",
    SAC_FLY = "Sac Fly",
    SAC_FLY_DOUBLE_PLAY = "Sac Fly Double Play",
    SINGLE = "Single",
    STOLEN_BASE_2B = "Stolen Base 2B",
    STOLEN_BASE_3B = "Stolen Base 3B",
    STOLEN_BASE_HOME = "Stolen Base Home",
    STRIKEOUT = "Strikeout",
    STRIKEOUT_DOUBLE_PLAY = "Strikeout Double Play",
    STRIKEOUT_TRIPLE_PLAY = "Strikeout Triple Play",
    TRIPLE = "Triple",
    TRIPLE_PLAY = "Triple Play",
    WALK = "Walk",
    WILD_PITCH = "Wild Pitch"
}

enum Base {
    FIRST = "1B",
    SECOND = "2B",
    THIRD = "3B",
    HOME = "4B"
}

enum MovementReason{
    R_ADV_FORCE = "r_adv_force",
    R_ADV_PLAY = "r_adv_play",
    R_ADV_THROW = "r_adv_throw",
    R_CAUGHT_STEALING_2B = "r_caught_stealing_2b",
    R_CAUGHT_STEALING_3B = "r_caught_stealing_3b",
    R_CAUGHT_STEALING_HOME = "r_caught_stealing_home",
    R_DEFENSIVE_INDIFF = "r_defensive_indiff",
    R_DOUBLED_OFF = "r_doubled_off",
    R_FORCE_OUT = "r_force_out",
    R_HBR = "r_hbr",
    R_INTERFERENCE = "r_interference",
    R_OUT_APPEAL = "r_out_appeal",
    R_OUT_RETURNING = "r_out_returning",
    R_OUT_STRETCHING = "r_out_stretching",
    R_PICKOFF_1B = "r_pickoff_1b",
    R_PICKOFF_2B = "r_pickoff_2b",
    R_PICKOFF_3B = "r_pickoff_3b",
    R_PICKOFF_CAUGHT_STEALING_2B = "r_pickoff_caught_stealing_2b",
    R_PICKOFF_CAUGHT_STEALING_3B = "r_pickoff_caught_stealing_3b",
    R_PICKOFF_CAUGHT_STEALING_HOME = "r_pickoff_caught_stealing_home",
    R_PICKOFF_ERROR_1B = "r_pickoff_error_1b",
    R_PICKOFF_ERROR_2B = "r_pickoff_error_2b",
    R_PICKOFF_ERROR_3B = "r_pickoff_error_3b",
    R_RUNDOWN = "r_rundown",
    R_RUNNER_OUT = "r_runner_out",
    R_STOLEN_BASE_2B = "r_stolen_base_2b",
    R_STOLEN_BASE_3B = "r_stolen_base_3b",
    R_STOLEN_BASE_HOME = "r_stolen_base_home",
    R_THROWN_OUT = "r_thrown_out"

}

export {
    Runner
}