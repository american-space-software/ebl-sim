import { Handedness } from "../handedness";
import { PlayEvent } from "./play-event";

interface Play {

    //relationships
    gameId?:number 

    //matchup
    hitterId?:string 
    batSide?:Handedness 

    pitcherId?:string 
    pitchHand?:Handedness 


    //about
    atBatIndex?: number
    
    halfInning?: HalfInning
    inning?: number
    
    isComplete?: boolean
    isScoringPlay?: boolean
    
    hasOut?: boolean

    //result
    event?:Event

    description?: string

    rbi?: number

    awayScore?:number
    homeScore?:number

    //count
    balls?: number
    strikes?: number
    outs?: number

    playEvents?:PlayEvent[]


    lastUpdated?:Date
    dateCreated?:Date

}

enum HalfInning {
    TOP = "top",
    BOTTOM = "bottom"
}




export {
    Play
}