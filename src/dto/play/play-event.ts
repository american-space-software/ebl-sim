import { Hardness, PitchType, PlayEventType, Trajectory } from "./enums";

interface PlayEvent {

    //relationships
    gameId?:number 
    playId?: number
    
    type?: PlayEventType

    //matchup
    hitterId?:string 
    batSide?:string 

    pitcherId?:string 
    pitchHand?:string 

    result?: {
        code?: string
        description?: string
        
        count: {
            balls?: number
            strikes?: number
            outs?: number
        }

        isInPlay?: boolean
        isStrike?: boolean
        isBall?: boolean
        isScoringPlay?: boolean
    }

    pitch?: {
        
        //pitch data
        pitchNumber?: number //Starts at 1. 
    
        pitchType?:PitchType

        pitchSpeed?: number    

        //Coordinates
        plateX?:number //Horizontal position of the ball when it crosses home plate from the catcher’s perspective.
        plateY?:number //Vertical position of the ball when it crosses home plate from the catcher’s perspective.

        zone?:number
    
        //hit data
        launchSpeed?: number
        launchAngle?: number

        totalDistance?: number
    
        trajectory?: Trajectory
        hardness?: Hardness
    
        location?: string
    
        coordX?: number
        coordY?: number

    }

    timestamp?:Date

}






export {
    PlayEvent
}