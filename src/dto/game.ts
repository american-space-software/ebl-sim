
interface Game {

    gameId:number

    status:string 

    season:string 
    type:string 

    gameDate:Date

    homeTeamId:number
    awayTeamId:number

    homeStartingPitcherId?:number
    awayStartingPitcherId?:number

    winningPitcherId?:number
    losingPitcherId?:number
    savePitcherId?:number

    weatherCondition?:string
    weatherTemp?:number 
    weatherWind?:string 
    
    awayTeamRuns?:number
    homeTeamRuns?:number

    awayTeamHits?:number
    homeTeamHits?:number
    
    awayTeamErrors?:number
    homeTeamErrors?:number

    lastUpdated?:Date
    dateCreated?:Date
}


export {
    Game
}