interface PitcherLog {

    pitcherId:number

    games:number
    starts:number
    wins:number
    losses:number
    saves:number
    bs:number

    outs:number
    er:number
    so:number
    h:number
    bb:number
    sho:number
    cg:number
    pc:number
    hbp:number

    singles:number
    doubles:number
    triples:number

    battersFace:number
    strikes:number
    balls:number
    runs:number
    hr:number

    era:number

    groundOuts:number
    flyOuts:number

    lineOuts:number
    groundBalls:number
    lineDrives:number
    flyBalls:number

    opponentObp:number
    opponentSlg:number
    opponentOps:number
    opponentIso:number
    opponentWOba:number
    opponentWFda:number
    opponentSoPercent:number

    siera:number
    xFip:number

    soPercent:number
    bbPercent:number
}

export {
    PitcherLog
}