interface Person {

    id: number

    firstName:string
    lastName:string

    primaryNumber:string

    birthDate?:Date

    active:boolean

    primaryPositionCode:string

    batSideCode:string
    pitchHandCode:string

    lastUpdated:Date
    dateCreated:Date
}

export {
    Person
}