import { Player } from "../dto/sim/player"
let csvToJson = require('convert-csv-to-json');




class PlayerDao {

    records:Player[]

    async setYear(year:number) {
        this.records = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv(`../data/hitters/${year}.csv`);
    }

    read(id:string):Player {

        for (let player of this.records) {
            if (player.id == id) return player
        }

    }

    list() : Player[] {
        return this.records
    }

}

export { PlayerDao }
