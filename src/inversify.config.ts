import { Container } from "inversify";
import { MainController } from "./main-controller";

let container:any

function getContainer() {

    if (container) return container 

    container  = new Container()
        
    container.bind(MainController).toSelf().inSingletonScope()

    return container
}



export {
    getContainer, container
}