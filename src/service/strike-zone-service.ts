import { injectable } from "inversify"
import { StrikeZone } from "../dto/strike-zone"

@injectable()
class StrikeZoneService {

    getDefault() {
        
        //Build strike zone
        let strikeZone:StrikeZone = {

            xMax: 100,
            xMin: -100,
            
            yMax: 100,
            yMin: -100,

            xStrikeMax: 50,
            xStrikeMin: -50,

            yStrikeMax: 50,
            yStrikeMin: -50

        }

        return strikeZone
        
    }

}

export {
    StrikeZoneService
}