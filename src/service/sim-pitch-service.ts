import { injectable } from "inversify";
import { resourceLimits } from "worker_threads";
import { Handedness } from "../dto/handedness";
import { PitchType } from "../dto/play/enums";
import { Play } from "../dto/play/play";
import { PlayEvent } from "../dto/play/play-event";
import { Player } from "../dto/player";
import { StrikeZone } from "../dto/strike-zone";
import { StrikeZoneService } from "./strike-zone-service";

@injectable()
class SimPitchService {

    constructor(
        private strikeZoneService:StrikeZoneService
    ) {}

    simPlateAppearance(hitter:Player, pitcher:Player) : Play {
        
        let play:Play = {}

        //Init count
        play.balls = 0
        play.strikes = 0

        //Pitcher info
        play.pitcherId = pitcher.personId
        play.pitchHand = pitcher.throwSide

        //Hitter info
        play.hitterId = hitter.personId

        play.playEvents = []

        if (hitter.batSide == Handedness.SWITCH) {

            //The opposite of the pitcher
            play.batSide = pitcher.throwSide == Handedness.LEFT ? Handedness.RIGHT : Handedness.LEFT

        } else {

            play.batSide = hitter.batSide

        }
        
        let playEnded = false

        while (!playEnded) {

            //Init playEvent
            let playEvent:PlayEvent = {
                hitterId: play.hitterId,
                pitcherId: play.pitcherId,
                batSide: play.batSide,
                pitchHand: play.pitchHand,
                pitch: {}
            }




            //Action?
            play.playEvents.push(this.simAction(playEvent))

            //Pickoff?
            play.playEvents.push(this.simPickoff(playEvent))

            //Pitch
            //User chooses pitch

            play.playEvents.push(this.simPitch(playEvent, PitchType.CHANGEUP, 5, 5))

        }
        

        return play

    }

    simPitch(playEvent:PlayEvent, pitchType:PitchType, targetX:number, targetY:number) : PlayEvent {

        //Pickoff? -- maybe happens outside of a pitch

        //Balk?

        let strikeZone:StrikeZone = this.strikeZoneService.getDefault()

        //Need pitch target

        //Choose type of pitch

        //Calculate release speed
            //Pitching power 
            //Pitch type
            //Pitcher fatigue


        //Calculate coordinates
            //Pitch type
            
            //Target coordinates

            //Apply stamina
            //Apply power
            //Apply movement
            //Apply control

        playEvent.pitch.pitchType = pitchType

        

        //Set all these values
            // result: {
            //     code?: string
            //     description?: string
                
            //     count: {
            //         balls?: number
            //         strikes?: number
            //         outs?: number
            //     }
        
            //     isInPlay?: boolean
            //     isStrike?: boolean
            //     isBall?: boolean
            //     isScoringPlay?: boolean
            // }
        
            // pitch?: {
                
            //     //pitch data
            //     pitchNumber?: number //Starts at 1. 
            
            //     startSpeed?: number
            //     endSpeed?: number
            //     strikeZoneTop?: number
            //     strikeZoneBottom?: number
                        
            //     //Coordinates
            //     aY?: number
            //     aZ?: number
            //     pfxX?: number
            //     pfxZ?: number
            //     pX?: number
            //     pZ?: number
            //     vX0?: number
            //     vY0?: number
            //     vZ0?: number
            //     x?: number
            //     y?: number
            //     x0?: number
            //     y0?: number
            //     z0?: number
            //     aX?: number
            
            //     //breaks
            //     breakAngle?: number
            //     breakLength?: number
            //     breakY?:number
            //     spinRate?: number
            //     spinDirection?: number
            
            //     zone?: number
            //     plateTime?: number
            //     extension?: number
            
            
            //     //hit data
            //     launchSpeed?: number
            //     launchAngle?: number
            //     totalDistance?: number
            
            //     trajectory?: Trajectory
            //     hardness?: Hardness
            
            //     location?: string
            
            //     coordX?: number
            //     coordY?: number
        
            // }

        return playEvent

    }

    simPickoff(playEvent:PlayEvent) : PlayEvent {
        return
    }

    simAction(playEvent:PlayEvent) : PlayEvent {

            //Ejection

            //Defensive substitution?

            //Offensive substitution? --ugh

            //Injury?

        return
    }

}

export {
    SimPitchService
}