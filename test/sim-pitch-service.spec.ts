
const assert = require('assert')
import { getTestContainer } from "./inversify.config"
import { Handedness } from "../src/dto/handedness"
import { Player } from "../src/dto/player"
import { SimPitchService } from "../src/service/sim-pitch-service"



let container

describe('SimPitchService', async () => {    

    let service: SimPitchService

    before('Main setup', async () => {

        container = await getTestContainer()
        service = container.get(SimPitchService)

    })


    it("should sim plate appearance", async () => {

       let hitter:Player = {
           player: {
               health: 85
           },

           hitting: {
                contact:75,
                power:65,
                gapPower:70,
                eye:90,
                
                speed:85, 
                baserunning:70,
        
                flyball:65, //higher is more flyballs
                pullHitter:65 //higher is more to same field
           },

           batSide: Handedness.LEFT,
           throwSide: Handedness.LEFT
       }

       let pitcher:Player = {
           
            pitching: {
                stamina:75,
                control:80,
                power:80,
                movement:75,

                flyball: 35
            }

       }

       service.simPlateAppearance(hitter, pitcher)

    })


})


