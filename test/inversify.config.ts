import "reflect-metadata"

import { Container } from "inversify";

import { SimPitchService } from "../src/service/sim-pitch-service"

let container


async function getTestContainer() {

    if (container) return container

    container = new Container()


    container.bind(SimPitchService).toSelf().inSingletonScope()



    return container

}




export {
    getTestContainer
}